//---------------------------------------------------------------------------

#ifndef Unit2H
#define Unit2H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Buttons.hpp>
//---------------------------------------------------------------------------
class TForm2 : public TForm
{
__published:	// IDE-managed Components
        TImage *Background;
        TLabel *Include;
        TLabel *Exclude;
        TEdit *Edit;
        TBitBtn *BOK;
        TBitBtn *BCancel;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall IncludeDblClick(TObject *Sender);
        void __fastcall ExcludeDblClick(TObject *Sender);
        void __fastcall BCancelClick(TObject *Sender);
        void __fastcall BOKClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
public:		// User declarations
        __fastcall TForm2(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm2 *Form2;
//---------------------------------------------------------------------------
#endif
