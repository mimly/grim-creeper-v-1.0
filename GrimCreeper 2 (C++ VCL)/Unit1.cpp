//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit2.h"
#include <string>
#include <cstring>
#include <iostream>
#include <windows.h>
#include <Winbase.h>
#include <Commctrl.h>
#include <Winuser.h>

#include <digitalv.h>
#include <mciavi.h>
#include <mmsystem.h>
#include <mmreg.h>
#include <msacm.h>
#include <vcr.h>
#include <vfw.h>

using namespace std;

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma link "CCALENDR"
#pragma link "CSPIN"
#pragma link "trayicon"
#pragma link "trayicon"
#pragma link "WMPLib_OCX"
#pragma link "AXVLC_OCX"
#pragma resource "*.dfm"

// TRAY CODE
//Show(); // it might be animated too...
//Application->BringToFront();

TForm1 *Form1;

int incl = 0, excl = 0;
bool backgroundIsLoaded = false, animationIsLoaded = false;
bool flag = false;
char the_path[MAX_PATH]; //origin path
bool crawlingStatus = false;
const int number = 5;
int properties[number] = {COLOR_HIGHLIGHT, COLOR_HIGHLIGHTTEXT,
                        COLOR_INFOBK, COLOR_INFOTEXT, COLOR_SCROLLBAR};
                        //COLOR_MENU, COLOR_MENUHILIGHT, COLOR_MENUBAR, COLOR_MENUTEXT,
                        //COLOR_WINDOW, COLOR_WINDOWFRAME, COLOR_WINDOWTEXT};
DWORD sysCls[number];

//---------------------------------------------------------------------------

__fastcall TForm1::TForm1(TComponent* Owner)
        : TForm(Owner)
{
}

void setStyle()
{
        //default colors
        for (int i = 0; i < number; i++)
        {
                sysCls[i] = GetSysColor(properties[i]);
        }
        //Salvadore Dali - surrealism :)
        DWORD gcCls[number];
        gcCls[0] = 0x0080FF80;
        gcCls[1] = clWhite;
        gcCls[2] = 0x0080FF80;
        gcCls[3] = clWhite;
        gcCls[4] = clYellow;
        //gcCls[5] = clWhite;
        //gcCls[6] = 0x0080FF80;
        //gcCls[7] = clWhite;
        //gcCls[8] = clBlack;
        //gcCls[9] = clWhite;
        //gcCls[10] = clWhite;
        //gcCls[11] = clBlack;
        SetSysColors(number, properties, gcCls);
}

//---------------------------------------------------------------------------

void setThePath()
{
        char origin[MAX_PATH];
        GetModuleFileName(NULL, origin, MAX_PATH); //origin path WIN XP+
        string::size_type pos = string(origin).find_last_of("\\/");
        ++pos;
        strncpy(the_path, origin, pos);
        the_path[pos] = '\0';
}

//---------------------------------------------------------------------------

void execute(const char* file)
{
        SHELLEXECUTEINFO ShellInfo;                     // Name structure
        memset(&ShellInfo, 0, sizeof(ShellInfo));       // Set up memory block
        ShellInfo.cbSize = sizeof(ShellInfo);           // Set up structure size
        ShellInfo.hwnd = Form1->Handle;                 // Calling window handle
        ShellInfo.lpVerb = "open";                      // Open the file with default program
        ShellInfo.lpFile = file;                        // File to open
        ShellInfo.nShow = SW_NORMAL;                    // Open in normal window
        ShellInfo.fMask = SEE_MASK_NOCLOSEPROCESS;      // Necessary if you want to wait for spawned process
        ShellExecuteEx(&ShellInfo);                     // Call to function
}

//---------------------------------------------------------------------------

void restore()
{
        incl = 0, excl = 0;
        Form1->LBInclude->Clear();
        Form1->LBExclude->Clear();
        Form1->LBWebsites->Clear();
        Form1->LBWebsites->Items->Add("DOMAIN");
        for (int i=1; i<11; i++)
        {
                Form1->LBInclude->Items->Add("INCLUDE KEYWORD #" + IntToStr(i));
                incl++;
                Form1->LBExclude->Items->Add("EXCLUDE KEYWORD #" + IntToStr(i));
                excl++;
                Form1->LBWebsites->Items->Add("WEBSITE #" + IntToStr(i));
        }

        Form1->DateTimePicker1->DateTime = Now();
        Form1->DateTimePicker2->DateTime = Now();
        Form1->inclExcl->Caption = IntToStr(incl) + " / " + IntToStr(excl);

        crawlingStatus = false;
        Form1->pw->Caption = "Please wait...";
        Form1->pw->Visible = false;
        Form1->pagesVisited->Caption = "READY";
        Form1->Timer1->Tag = 1;
        Form1->Timer1->Enabled = false;
        Form1->BCrawl->Glyph->LoadFromFile("img/crawl.bmp");
        Form1->ProgressBar1->Position = 0;
}

//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
        setThePath();

        //Application->HelpFile = "docs/manual.chm"; not supported by Windows 10+...

        restore();
        SendMessage(ProgressBar1->Handle, PBM_SETBARCOLOR, 0, 0x0080FF80);
        setStyle();

        // Load URL:s button (LoadFromFile) changes the path which leads to
        // the inability to find the background file while restoring the application.
        if (!backgroundIsLoaded)
        {
                Background->Picture->LoadFromFile("img/background.bmp");
                backgroundIsLoaded = true;
        }

        if (!animationIsLoaded)
        {
                try {
                        wchar_t url2[100];
                        AnsiString url = "avi\animation.avi";
                        url.WideChar(url2,100);
                        WindowsMediaPlayer1->launchURL(url2);
                        //WindowsMediaPlayer1->Visible = false;
                        animationIsLoaded = true;
                } catch (...) {
                        ShowMessage("There was some kind of problem with the animated intro.");
                        WindowsMediaPlayer1->Visible = false;
                }
        }
}

//---------------------------------------------------------------------------

void __fastcall TForm1::WindowsMediaPlayer1PlayStateChange(TObject *Sender,
      long NewState)
{
        if (flag) WindowsMediaPlayer1->Visible = false;
        flag = true;
}

//---------------------------------------------------------------------------

void __fastcall TForm1::WindowsMediaPlayer1_Click(TObject *Sender,
      short nButton, short nShiftState, long fX, long fY)
{
        WindowsMediaPlayer1->close();
}

//---------------------------------------------------------------------------

void __fastcall TForm1::useCurrentTimeDblClick(TObject *Sender)
{
        if (DateTimePicker2->Enabled == true)
        {
                DateTimePicker2->DateTime = Now();
                DateTimePicker2->Enabled = false;
        } else
        {
                DateTimePicker2->Enabled = true;
        }
}

//---------------------------------------------------------------------------

void __fastcall TForm1::BAddClick(TObject *Sender)
{
        Form2->ShowModal();
}

//---------------------------------------------------------------------------

void __fastcall TForm1::BRemoveClick(TObject *Sender)
{
        if (LBInclude->SelCount == 0 && LBExclude->SelCount == 0) {
                ShowMessage("Please select some keyword(s) first.");
        } else {
                LBInclude->DeleteSelected();
                LBExclude->DeleteSelected();
                incl = LBInclude->Items->Count;
                excl = LBExclude->Items->Count;
                inclExcl->Caption = IntToStr(incl) + " / " + IntToStr(excl);
        }
}

//---------------------------------------------------------------------------

void __fastcall TForm1::BLoadClick(TObject *Sender)
{
        if (OpenDialog1->Execute())
        {
                try
                {
                        LBWebsites->Items->LoadFromFile(OpenDialog1->FileName);
                        SetCurrentDirectory(the_path);
                }
                catch (...)
                {
                        ShowMessage("Bad bad file!!");
                }
        }
}

//---------------------------------------------------------------------------

void __fastcall TForm1::LBIncludeDrawItem(TWinControl *Control, int Index,
      TRect &Rect, TOwnerDrawState State)
{
        TListBox *pListbox = dynamic_cast<TListBox*>(Control);
        TCanvas *pCanvas = pListbox->Canvas;

        if(State.Contains(odSelected))
        {
                pCanvas->Brush->Color = clWhite;
                pCanvas->Font->Color = clBlack;
        }

        pCanvas->FillRect(Rect);
        pCanvas->TextRect(Rect, Rect.Left,Rect.Top, pListbox->Items->Strings[Index]);
}

//---------------------------------------------------------------------------

void __fastcall TForm1::LBWebsitesDrawItem(TWinControl *Control, int Index,
      TRect &Rect, TOwnerDrawState State)
{
        TListBox *pListbox = dynamic_cast<TListBox*>(Control);
        TCanvas *pCanvas = pListbox->Canvas;

        if(State.Contains(odSelected) && Index == 0)
        {
                pCanvas->Brush->Color = 0x0080FF80;
                pCanvas->Font->Color = clBlack;
        }
        else if(State.Contains(odSelected))
        {
                pCanvas->Brush->Color = clWhite;
                pCanvas->Font->Color = clBlack;
        }
        else if (Index == 0)
        {
                pCanvas->Brush->Color = 0x0080FF80;
                pCanvas->Font->Color = clBlack;
        }

        pCanvas->FillRect(Rect);
        pCanvas->TextRect(Rect, Rect.Left,Rect.Top, pListbox->Items->Strings[Index]);
}

//---------------------------------------------------------------------------

void __fastcall TForm1::LBWebsitesClick(TObject *Sender)
{
        if (LBWebsites->ItemIndex == 0)
        {
                LBWebsites->ItemIndex = 1;
                LBWebsites->Selected[1] = true;
        }
}

//---------------------------------------------------------------------------

void __fastcall TForm1::BCrawlClick(TObject *Sender)
{
        if (!crawlingStatus) // Let's crawl!
        {
                SendMessage(ProgressBar1->Handle, PBM_SETBARCOLOR, 0, 0x0080FF80);
                crawlingStatus = true;
                pw->Visible = true;
                if (pw->Caption == "Completed.") pw->Caption = "Please wait...";
                BCrawl->Glyph->LoadFromFile("img/stop.bmp");
                Form1->Timer1Timer(Sender);
        }
        else // Stop crawling.
        {
                crawlingStatus = false;
                pw->Visible = false;
                Timer1->Enabled = false;
                BCrawl->Glyph->LoadFromFile("img/crawl.bmp");
        }
}

//---------------------------------------------------------------------------

void __fastcall TForm1::Timer1Timer(TObject *Sender)
{
        if (ProgressBar1->Position == 100) {
                Timer1->Enabled = false;
                SendMessage(ProgressBar1->Handle, PBM_SETBARCOLOR, 0, clBlack);
                //SendMessage(ProgressBar1->Handle, PBM_SETSTATE, 0, PBST_PAUSED);
                //SendMessage(ProgressBar1->Handle, PBM_SETMARQUEE, 0, PBS_MARQUEE);
                pw->Caption = "Completed.";
                BCrawl->Glyph->LoadFromFile("img/crawl.bmp");
                ProgressBar1->Position = 0;
                Timer1->Tag = 1;
                crawlingStatus = false;
        } else {
                Timer1->Enabled = true;
                // Get the current time
	        TDateTime time = TDateTime::CurrentTime();
	        unsigned short Hours, Minutes, Seconds, Milliseconds;

	        // Retrieve the hour, minute, and second values of the current time
	        time.DecodeTime(&Hours, &Minutes, &Seconds, &Milliseconds);

                ProgressBar1->StepBy(3*100/60);

	        pagesVisited->Caption = IntToStr(Timer1->Tag++) + " pages visited.";
        }
}

//---------------------------------------------------------------------------

void __fastcall TForm1::Restore1Click(TObject *Sender)
{
        restore();
}

//---------------------------------------------------------------------------

void __fastcall TForm1::FormClose(TObject *Sender, TCloseAction &Action)
{
        //Form1->Close(); //it will close only one window
        if (Application->MessageBoxA("Do You really want to quit?", "Confirm", MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2 | MB_TASKMODAL)
        == IDNO) {
                Action=caNone;
        }
}

//---------------------------------------------------------------------------

void __fastcall TForm1::Close1Click(TObject *Sender)
{
        //Form1->Close(); //it will close only one window
        if (Application->MessageBoxA("Do You really want to quit?", "Confirm", MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2 | MB_TASKMODAL)
        == IDYES) {
                SetSysColors(number, properties, sysCls);
                Application->Terminate();
        }
}

//---------------------------------------------------------------------------

void __fastcall TForm1::AddKeyword1Click(TObject *Sender)
{
        Form1->BAddClick(Sender);
}

//---------------------------------------------------------------------------

void __fastcall TForm1::RemoveKeyword1Click(TObject *Sender)
{
        Form1->BRemoveClick(Sender);
}

//---------------------------------------------------------------------------

void __fastcall TForm1::LoadURLs1Click(TObject *Sender)
{
        Form1->BLoadClick(Sender);
}

//---------------------------------------------------------------------------

void __fastcall TForm1::Manual1Click(TObject *Sender)
{
        execute("docs\\manual.chm");
        //Application->HelpCommand(HELP_CONTENTS,0); not supported by Windows 10+...
}

//---------------------------------------------------------------------------

void __fastcall TForm1::SRS1Click(TObject *Sender)
{
        execute("docs\\SRS1.pdf");
}

//---------------------------------------------------------------------------

void __fastcall TForm1::About1Click(TObject *Sender)
{
        Application->MessageBoxA("Grim Creeper 1.0 \n\nby Dekagon, 2017 \n\nPlease check the manual or the software \nrequirements specification for further information.",
         "About...", MB_OK | MB_ICONINFORMATION | MB_DEFBUTTON1 | MB_TASKMODAL);
}

//---------------------------------------------------------------------------

void __fastcall TForm1::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
        if (Shift.Contains(ssCtrl)) {
                if ((Key == 'w') || (Key == 'W')) {
                        Form1->Restore1Click(Sender);
                }
                else if ((Key == 'a') || (Key == 'A')) {
                        Form1->BAddClick(Sender);
                }
                else if ((Key == 'r') || (Key == 'R')) {
                        Form1->BRemoveClick(Sender);
                }
                else if ((Key == 'l') || (Key == 'L')) {
                        Form1->BLoadClick(Sender);
                }
                else if ((Key == 'q') || (Key == 'Q')) {
                        Form1->Close1Click(Sender);
                }
                else if ((Key == 'm') || (Key == 'M')) {
                        Form1->Manual1Click(Sender);
                }
                else if ((Key == 's') || (Key == 'S')) {
                        Form1->SRS1Click(Sender);
                }
        } else if (Key == VK_F1) {
                Form1->About1Click(Sender);
        }
}

//---------------------------------------------------------------------------

void __fastcall TForm1::FormKeyUp(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
        if (Key == VK_DELETE) {
                Form1->BRemoveClick(Sender);
        } else if (Key == VK_ESCAPE) {
                Form1->Close1Click(Sender);
        }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormKeyPress(TObject *Sender, char &Key)
{
        if (Key == VK_DELETE) {
                Form1->BRemoveClick(Sender);
        } else if (Key == VK_ESCAPE) {
                Form1->Close1Click(Sender);
        }
}

//---------------------------------------------------------------------------

void __fastcall TForm1::LBIncludeKeyUp(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
        if (Key == VK_DELETE) {
                Form1->BRemoveClick(Sender);
        } else if (Key == VK_ESCAPE) {
                Form1->Close1Click(Sender);
        }
}

//---------------------------------------------------------------------------

