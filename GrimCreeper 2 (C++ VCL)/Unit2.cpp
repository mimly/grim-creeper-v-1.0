//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit2.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)
#pragma resource "*.dfm"

TForm2 *Form2;

bool include = true;;

//---------------------------------------------------------------------------

__fastcall TForm2::TForm2(TComponent* Owner)
        : TForm(Owner)
{
}

//---------------------------------------------------------------------------

void __fastcall TForm2::FormCreate(TObject *Sender)
{
        Background->Picture->LoadFromFile("img/background.bmp");
        Edit->Clear();
        if (include) {
                Edit->Font->Color = 0x0080FF80;
                Include->Font->Size = 11;
                Exclude->Font->Size = 9;
        }
}

//---------------------------------------------------------------------------

void __fastcall TForm2::IncludeDblClick(TObject *Sender)
{
        include = true;
        Include->Font->Size = 11;
        Exclude->Font->Size = 9;
        Edit->Font->Color = 0x0080FF80;
}

//---------------------------------------------------------------------------

void __fastcall TForm2::ExcludeDblClick(TObject *Sender)
{
        include = false;
        Exclude->Font->Size = 11;
        Include->Font->Size = 9;
        Edit->Font->Color = clRed;
}

//---------------------------------------------------------------------------

void __fastcall TForm2::BCancelClick(TObject *Sender)
{
        Form2->Close();
}

//---------------------------------------------------------------------------

void __fastcall TForm2::BOKClick(TObject *Sender)
{
        if (Edit->Text == "") ShowMessage("The field is empty! Please type some keyword.");
        else if (include) {
                Form1->LBInclude->Items->Add(Edit->Text); //add to LBInclude
                Form2->Close();
        } else {
                Form1->LBExclude->Items->Add(Edit->Text); //add to LBExclude
                Form2->Close();
        }
}

//---------------------------------------------------------------------------

void __fastcall TForm2::FormClose(TObject *Sender, TCloseAction &Action)
{
        Edit->Clear();
}

//---------------------------------------------------------------------------

