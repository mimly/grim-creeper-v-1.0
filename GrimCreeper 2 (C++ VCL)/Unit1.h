//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <Dialogs.hpp>
#include <Buttons.hpp>
#include "trayicon.h"
#include <MPlayer.hpp>
#include "WMPLib_OCX.h"
#include <OleCtrls.hpp>
#include "AXVLC_OCX.h"
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
        TImage *Background;
        TLabel *from;
        TDateTimePicker *DateTimePicker1;
        TLabel *until;
        TDateTimePicker *DateTimePicker2;
        TListBox *LBInclude;
        TListBox *LBExclude;
        TLabel *inclExcl;
        TListBox *LBWebsites;
        TProgressBar *ProgressBar1;
        TLabel *timeFrame;
        TLabel *keywords;
        TLabel *websites;
        TLabel *useCurrentTime;
        TOpenDialog *OpenDialog1;
        TTimer *Timer1;
        TLabel *pagesVisited;
        TLabel *pw;
        TMainMenu *MainMenu1;
        TMenuItem *File1;
        TMenuItem *Options1;
        TMenuItem *Help1;
        TMenuItem *Settings1;
        TMenuItem *Documentation1;
        TMenuItem *N1;
        TMenuItem *About1;
        TMenuItem *Manual1;
        TMenuItem *SRS1;
        TMenuItem *Restore1;
        TMenuItem *N2;
        TMenuItem *Import1;
        TMenuItem *Export1;
        TMenuItem *N3;
        TMenuItem *AddKeyword1;
        TMenuItem *RemoveKeyword1;
        TMenuItem *LoadURLs1;
        TMenuItem *N4;
        TMenuItem *Close1;
        TBitBtn *BCrawl;
        TWindowsMediaPlayer *WindowsMediaPlayer1;
        TBitBtn *BAdd;
        TBitBtn *BRemove;
        TBitBtn *BLoad;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall WindowsMediaPlayer1PlayStateChange(TObject *Sender,
          long NewState);
        void __fastcall WindowsMediaPlayer1_Click(TObject *Sender,
          short nButton, short nShiftState, long fX, long fY);
        void __fastcall useCurrentTimeDblClick(TObject *Sender);
        void __fastcall BAddClick(TObject *Sender);
        void __fastcall BRemoveClick(TObject *Sender);
        void __fastcall BLoadClick(TObject *Sender);
        void __fastcall LBIncludeDrawItem(TWinControl *Control, int Index,
          TRect &Rect, TOwnerDrawState State);
        void __fastcall LBWebsitesDrawItem(TWinControl *Control, int Index,
          TRect &Rect, TOwnerDrawState State);
        void __fastcall LBWebsitesClick(TObject *Sender);
        void __fastcall BCrawlClick(TObject *Sender);
        void __fastcall Timer1Timer(TObject *Sender);
        void __fastcall Restore1Click(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall Close1Click(TObject *Sender);
        void __fastcall AddKeyword1Click(TObject *Sender);
        void __fastcall RemoveKeyword1Click(TObject *Sender);
        void __fastcall LoadURLs1Click(TObject *Sender);
        void __fastcall Manual1Click(TObject *Sender);
        void __fastcall SRS1Click(TObject *Sender);
        void __fastcall About1Click(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall FormKeyUp(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall FormKeyPress(TObject *Sender, char &Key);
        void __fastcall LBIncludeKeyUp(TObject *Sender, WORD &Key,
          TShiftState Shift);
private:	// User declarations
public:		// User declarations
        __fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
